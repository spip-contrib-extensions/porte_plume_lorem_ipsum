<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'pp_loremipsum_description' => 'Cette extension ajoute des boutons pour créer du texte factice dans la barre d\'outil de SPIP.',
	'pp_loremipsum_nom' => 'Lorem Ipsum pour Porte Plume',
	'pp_loremipsum_slogan' => 'Des boutons pour créer du texte factice',
);
