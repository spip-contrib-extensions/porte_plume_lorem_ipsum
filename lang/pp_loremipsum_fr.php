<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/porte_plume_extras/codes/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'outil_texte_lorem_ipsum_1' => "Insérer 1 paragraphe de faux texte latin",
	'outil_texte_lorem_ipsum_3' => "Insérer 3 paragraphes de faux texte latin",

);
