# Changelog

## 1.1.7 - 2023-06-07

### Changed

- Compatibilité SPIP 4.2
- ajout d'un CHANGELOG.md et d'un README.md